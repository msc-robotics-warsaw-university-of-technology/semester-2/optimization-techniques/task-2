function K = kernel(z, y, d, kern, varargin)
    
    if strcmp(kern,'gaussianGamma')
        gamma = d;
        if length(varargin) == 1
            gamma=varargin{2};
        end
    elseif strcmp(kern,'sigmoidfunction')
        sigma = 1/d;
        if length(varargin) == 1
            sigma=varargin{2};
        end
    end
    
    
    if strcmp(kern,'linear')
        K = (z*y');
    elseif strcmp(kern,'polyHomogenous')
        K = (z*y').^d;
    elseif strcmp(kern,'polyNonHomogenous')
        K = (z*y'+1).^d;
    elseif strcmp(kern,'gaussianGamma')
        n = size(z,1); 
        m = size(y,1);
        K=zeros(n,m);
        for i=1:n
            for j=1:m
                K(i,j)=exp(-gamma*norm(z(i,:)-y(j,:))^2);
            end
        end
    elseif strcmp(kern,'sigmoidfunction')
        n = size(z,1); 
        m = size(y,1);
        K=zeros(n,m);
        for i=1:n
            for j=1:m
                K(i,j)=exp(-(1/sigma)*norm(z(i,:)-y(j,:)));
            end
        end
    else
        error('Invalid kernel type')
    end
end

