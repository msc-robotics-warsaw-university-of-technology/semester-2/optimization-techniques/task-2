function shuffledData = loadData(url, sheet)

    Data = urlToMatrix(url, sheet);
    
    index_Very_Low = Data(:,6) == 1;
    index_Low = Data(:,6) == 2;
    index_Medium = Data(:,6) == 3;
    index_High = Data(:,6) == 4;
    
    % Separate the Data Based on classes
    
    Very_Low = Data(index_Very_Low,:);
    Low = Data(index_Low,:);
    Medium = Data(index_Medium,:);
    High = Data(index_High,:);
    
    % Take First 24 of All Classes 
    
    Very_Low = Very_Low(1:24,:);
    Low = Low(1:24,:);
    Medium = Medium(1:24,:);
    High = High(1:24,:);
    
    % Combine all the classes

    DataCombined=[  
                    %Very_Low;
                    %Low;
                    Medium;
                    High
                    ];

    [m,~] = size(DataCombined);
    shuffledRow = randperm(m);
    
    shuffledData = DataCombined(shuffledRow, :);

end










