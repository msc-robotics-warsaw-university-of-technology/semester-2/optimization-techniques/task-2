function loadSaveData(url)

    Train_Data = loadData(url,'Training_Data');
    Test_Data = loadData(url,'Test_Data');
    
    save('Train_Data.mat','Train_Data');
    save('Test_Data.mat','Test_Data');
end

