clc;
clear;
close all;

url ='https://archive.ics.uci.edu/ml/machine-learning-databases/00257/Data_User_Modeling_Dataset_Hamdi%20Tolga%20KAHRAMAN.xls';

if ~(isfile('Test_Data.mat') && isfile('Train_Data.mat'))
    loadSaveData(url);
end
%% Load Data

[X_Train, Y_Train, X_Test, Y_Test] = splitData();

% Change the classes to -1 and 1

a = max(Y_Train);

Y_Train(Y_Train ~= a) = 1;
Y_Train(Y_Train == a) = -1;

Y_Test(Y_Test ~= a) = 1;
Y_Test(Y_Test == a) = -1;

[r,c]=size(X_Train);

%% Hard Margin 

% Generating Optimization parameters

[H,f,A,b,Aeq,beq,lb,ub,x0] = optimizing_parameters('primalHardMargin', ...
    X_Train,Y_Train);

Y_Pred_Hard_Margin = mySVC(H,f,A,b,Aeq,beq,lb,ub,x0, X_Test, 'primal');

Accuracy = sum(Y_Pred_Hard_Margin == Y_Test) / length(Y_Test)*100;

fprintf('The accuracy using Primal Hard Margin is %.2f %% \n',Accuracy);

%% Soft Margin

% Generating Optimization parameters

[H,f,A,b,Aeq,beq,lb,ub,x0] = optimizing_parameters('primalSoftMargin', ...
    X_Train,Y_Train);

Y_Pred_Soft_Margin = mySVC(H,f,A,b,Aeq,beq,lb,ub,x0, X_Test,'primal');

Accuracy = sum(Y_Pred_Soft_Margin == Y_Test) / length(Y_Test)*100;

fprintf('The accuracy using Primal Soft Margin is %.2f %% \n',Accuracy);

%% Dual Linear

% Generating Optimization parameters

[H,f,A,b,Aeq,beq,lb,ub,x0,K] = optimizing_parameters('dual',X_Train, ...
    Y_Train,'kernel','linear');

Y_Pred_Dual_Linear = mySVC(H,f,A,b,Aeq,beq,lb,ub,x0, X_Test, 'dual', K, ...
    X_Train, Y_Train,'linear');

Accuracy = sum(Y_Pred_Dual_Linear == Y_Test) / length(Y_Test)*100;

fprintf('The accuracy using Dual Linear is %.2f %% \n',Accuracy);

%% Dual Polynomial homogeneous

% Generating Optimization parameters

[H,f,A,b,Aeq,beq,lb,ub,x0,K] = optimizing_parameters('dual',X_Train, ...
    Y_Train,'kernel','polyHomogenous');

Y_Pred_DualPolyHomogeneous = mySVC(H,f,A,b,Aeq,beq,lb,ub,x0, X_Test, ...
    'dual', K, X_Train, Y_Train,'polyHomogenous');

Accuracy = sum(Y_Pred_DualPolyHomogeneous == Y_Test) / length(Y_Test)*100;

fprintf('The accuracy using Dual Polynomial Homogeneous is %.2f %% \n',Accuracy);


%% Polynomial nonhomogeneous

% Generating Optimization parameters

[H,f,A,b,Aeq,beq,lb,ub,x0,K] = optimizing_parameters('dual',X_Train, ...
    Y_Train,'kernel','polyNonHomogenous');

Y_Pred_DualPolyNonHomogeneous=mySVC(H,f,A,b,Aeq,beq,lb,ub,x0, X_Test, ...
    'dual', K, X_Train, Y_Train,'polyNonHomogenous');

Accuracy = sum(Y_Pred_DualPolyNonHomogeneous == Y_Test)/length(Y_Test)*100;

fprintf('The accuracy using Dual Polynomial Non-Homogeneous is %.2f %% \n',Accuracy);

%% Gaussian radial basis function with gamma

% Generating Optimization parameters

[H,f,A,b,Aeq,beq,lb,ub,x0,K] = optimizing_parameters('dual',X_Train, ...
    Y_Train,'kernel','gaussianGamma');

[Y_Pred_Dual_Gaussian_G] = mySVC(H,f,A,b,Aeq,beq,lb,ub,x0, X_Test, ...
    'dual', K, X_Train, Y_Train, 'gaussianGamma');

Accuracy = sum(Y_Pred_Dual_Gaussian_G == Y_Test) / length(Y_Test)*100;

fprintf('The accuracy using Dual Gaussian radial basis function with gamma is %.2f %% \n',Accuracy);

%% Gaussian radial basis function with sigma

% Generating Optimization parameters

[H,f,A,b,Aeq,beq,lb,ub,x0,K] = optimizing_parameters('dual',X_Train, ...
    Y_Train,'kernel','sigmoidfunction');

Y_Pred_Dual_Gaussian_S = mySVC(H,f,A,b,Aeq,beq,lb,ub,x0, X_Test, ...
    'dual', K, X_Train, Y_Train,'sigmoidfunction');
    
Accuracy = sum(Y_Pred_Dual_Gaussian_S == Y_Test) / length(Y_Test)*100;

fprintf('The accuracy using Dual Sigmoid function with sigma is %.2f %% \n',Accuracy);

%% Delete saved file if required

del = input('Do you want to remove the save file y/n: ','s');

if del == 'y'
    delete Test_Data.mat
    delete Train_Data.mat
end