function [Y_pred] = mySVC(H,f,A,b,Aeq,beq,lb,ub,x0, X_Test, type, varargin)
[r, c]=size(X_Test);

if strcmp(type,'dual')
    if length(varargin) == 3
        kern = 'linear';
    elseif length(varargin) == 4
        kern = varargin{4};
    else
        error('Wrong number of additional argument')
    end
    K = varargin{1};
    X_Train = varargin{2};
    Y_Train = varargin{3};
end
switch type
    case 'primal'
        %Quadratic minimization of with respect to alpha
        w = quadprog(H,f,A,b,Aeq,beq,lb,ub,x0,optimoptions('quadprog','Display','off'));
        
        Y_pred=((X_Test)*w(1:c)) + w(c+1);
        
        Y_pred(Y_pred<0)=-1;
        Y_pred(Y_pred>0)=1;
    case 'dual'
        %Quadratic minimization of with respect to alpha
        alpha = quadprog(H,f,A,b,Aeq,beq,lb,ub,x0,optimoptions('quadprog','Display','off'));
        id = find(alpha ~= 0);
        b = 0;
        for i=1:length(id)
            b = b + Y_Train(i) - ((alpha.* Y_Train)' * K(:,i));
        end
        b = b / length(id);

        Y_pred = zeros(r,1);
        for i=1:length(X_Test)
            Y_pred(i) = sign(((alpha.* Y_Train)' * kernel(X_Train,...
                X_Test(i,:), 0.0001, kern) + b) / 2);
        end
end

end

