function [H,f,A,b,Aeq,beq,lb,ub,x0,K] = optimizing_parameters(type, Z, y, varargin)
    
    [r, c] = size(Z);
    C = 1;
    d = 0.0001;
    kern = 'linear';

    for i=1:length(varargin)
        if strcmp(varargin{i},'C')
            C=varargin{i+1}; 
        elseif strcmp(varargin{i},'kernel')
            kern = varargin{i+1};
        end
    end

    switch type
        case 'primalHardMargin'
            f = zeros(c+1,1);
            H = eye(c+1);
            H(c+1,c+1) = 0;
            A = -diag(y)*([Z, ones(r,1)]);
            b = -ones(r,1);
            Aeq = [];
            beq = [];
            lb = [];
            ub = [];
            x0 = [];

        case 'primalSoftMargin'
            H = zeros(c+r+1);
            H(boolean(eye(r+c+1)))=[ones(1,r),zeros(1,c+1)];
            f = [zeros(1,c+1), (C/r)*ones(1,r)];
            Aeq = [];
            beq = [];
            lb = [-inf.*ones(1,c+1), zeros(1,r)]';
            ub = [];
            x0 = [];
            A = -[diag(y)*[Z,ones(r,1)], eye(r)];
            b = -ones(r,1);

        case 'dual'
            K = kernel(Z, Z, d,kern);
            H = diag(y)*K*diag(y);
            f = -ones(r,1);
            Aeq = y';
            beq = 0;
            lb = zeros(r,1);
            ub = C * ones(r,1);
            x0 = [];
            A = [];
            b = [];
        otherwise
            error('Select the proper optimizing parameter')
    end
end

