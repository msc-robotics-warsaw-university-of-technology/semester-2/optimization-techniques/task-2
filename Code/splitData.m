function [X_Train, Y_Train, X_Test, Y_Test] = splitData()

load('Train_Data.mat')
load('Test_Data.mat');

X_Train = Train_Data(:,1:5);
Y_Train = Train_Data(:,6);

X_Test = Test_Data(:,1:5);
Y_Test = Test_Data(:,6);

end

