function A = urlToMatrix(url, sheet)

    x=readtable(url,'Sheet',sheet);
    x=x(:,(1:6));    

    x.UNS(strcmpi(x.UNS,'very_low')|strcmpi(x.UNS,'Very Low')) = {1}; 
    x.UNS(strcmpi(x.UNS,'Low')) = {2}; 
    x.UNS(strcmpi(x.UNS,'Middle')) = {3}; 
    x.UNS(strcmpi(x.UNS,'High')) = {4}; 
    
    A = cell2mat(table2cell(x));
    
end